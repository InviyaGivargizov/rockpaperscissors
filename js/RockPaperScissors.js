/**
 * Created by Invia on 15.03.2017.
 */

function resetJS() {
    $(".rockrob").css("display", "block");
    $(".paperrob").css("display", "block");
    $(".scissorrob").css("display", "block");
    $(".result").html("WAIT FOR CHOICE!!!");
    $(".result").css("color", "green");
}


function rockJS () {
    var items = [1, 2, 3];
    var rand = items[Math.floor(Math.random() * items.length)];
    var user = 2;
    function draw (){
                     $(".rockrob").css("display", "block");
                     $(".paperrob").css("display", "none");
                     $(".scissorrob").css("display", "none");
                     $(".result").html("D R A W !!!");
                     $(".result").css("color", "yellow");
    };
    function win() {
                    $(".rockrob").css("display", "none");
                    $(".paperrob").css("display", "none");
                    $(".scissorrob").css("display", "block");
                    $(".result").html("WIN !!!");
                    $(".result").css("color", "blue");
    };
    function lose() {
                    $(".rockrob").css("display", "none");
                    $(".paperrob").css("display", "block");
                    $(".scissorrob").css("display", "none");
                    $(".result").html("LOSE !!!");
                    $(".result").css("color", "red");
    };

    if (user === rand) {
        return draw ();
    }
    else if (user > rand) {
        return win ();
    }
    else {
        return lose();
    }
}

function paperJS () {
    var items = [1, 2, 3];
    var rand = items[Math.floor(Math.random() * items.length)];
    var user = 2;
    function draw (){
        $(".rockrob").css("display", "none");
        $(".paperrob").css("display", "block");
        $(".scissorrob").css("display", "none");
        $(".result").html("D R A W !!!");
        $(".result").css("color", "yellow");
    };
    function win() {
        $(".rockrob").css("display", "block");
        $(".paperrob").css("display", "none");
        $(".scissorrob").css("display", "none");
        $(".result").html("WIN !!!");
        $(".result").css("color", "blue");
    };
    function lose() {
        $(".rockrob").css("display", "none");
        $(".paperrob").css("display", "none");
        $(".scissorrob").css("display", "block");
        $(".result").html("LOSE !!!");
        $(".result").css("color", "red");
    };

    if (user === rand) {
        return draw ();
    }
    else if (user > rand) {
        return win ();
    }
    else {
        return lose();
    }
}
function scissorJS () {
    var items = [1, 2, 3];
    var rand = items[Math.floor(Math.random() * items.length)];
    var user = 2;
    function draw (){
        $(".rockrob").css("display", "none");
        $(".paperrob").css("display", "none");
        $(".scissorrob").css("display", "block");
        $(".result").html("D R A W !!!");
        $(".result").css("color", "yellow");
    };
    function win() {
        $(".rockrob").css("display", "none");
        $(".paperrob").css("display", "block");
        $(".scissorrob").css("display", "none");
        $(".result").html("WIN !!!");
        $(".result").css("color", "blue");
    };
    function lose() {
        $(".rockrob").css("display", "block");
        $(".paperrob").css("display", "none");
        $(".scissorrob").css("display", "none");
        $(".result").html("LOSE !!!");
        $(".result").css("color", "red");
    };

    if (user === rand) {
        return draw ();
    }
    else if (user > rand) {
        return win ();
    }
    else {
        return lose();
    }
}
